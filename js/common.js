/**
 * Created by Administrator on 2017/1/14.
 */
function getRandom(a, b) {
    if (b > a) {
        var t = a;
        a = b;
        b = t;
    }
    return Math.round(Math.random()*(b-a))+a;
}

function getRandomColor(){
    var cs = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'];
    var c = '#';
    c+=cs[getRandom(0,cs.length)];
    c+=cs[getRandom(0,cs.length)];
    c+=cs[getRandom(0,cs.length)];
    c+=cs[getRandom(0,cs.length)];
    c+=cs[getRandom(0,cs.length)];
    c+=cs[getRandom(0,cs.length)];
    return c;
}

function getTimer(id){
    var d = new Date();
    var y = d.getFullYear();
    var m = d.getMonth()+1;
    if(m<10) m = '0'+m;
    var dd = d.getDate();
    if(dd<10) dd = '0'+dd;

    var hh = d.getHours();
    if(hh<10) hh = '0'+hh;

    var mm = d.getMinutes();
    if(mm<10) mm = '0'+mm;

    var ss = d.getSeconds();
    if(ss<10) ss = '0'+ss;
    document.getElementById(id).innerHTML = y+"年"+m+"月"+dd+"日 "+hh+":"+mm+":"+ss+" 星期"+"日一二三四五六".charAt(d.getDay());
}